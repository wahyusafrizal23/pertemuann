Ext.define('Pertemuan.view.form.FormLogin', {
    extend: 'Ext.form.Panel',
    

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password',
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'login',
    id:'login',
    controller: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Login Form',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'Userame',
                    placeHolder: 'Tom Roy',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    hasDisabled: false,
                    handler: 'onLogin'
                }
                
            ]
        }
    ]
});