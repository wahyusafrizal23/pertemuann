/**
 * Demonstrates how to use an Ext.Carousel in vertical and horizontal configurations
 */
Ext.define('Pertemuan.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousle',

    requires: [
        'Ext.carousel.Carousel'
    ],

    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   /** defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },*/ 
    items: [{
        xtype: 'carousel',
        width:100,
        items: [{
            html: '<p>Swipe left to show the next card…</p>',
            
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>',
            
        },
        {
            html: 'Card #3',
            
        }]
    },{
        xtype: 'spacer'
    }, {
        xtype: 'carousel',
        width:200,
        ui: 'light',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
            
        },
        {
            html: 'And can also use <code>ui:light</code>.',
           
        },
        {
            html: 'Card #3',
         
        }]
    }]
});