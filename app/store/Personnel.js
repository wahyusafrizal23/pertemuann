Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'photo','npm','name', 'email', 'phone','hobi'
    ],

    data: { items: [
        { photo: 'resources/5.jpg',npm: '183510512',name: 'Wahyu Safrizal', email: "wahyusafrizal23@student.uir.ac.id", phone: "555-111-1111", hobi:"Games"},
        { photo: 'resources/lg.png',npm: '193510513',name: 'Windi', email: "windi.moghsson@student.uir.ac.id",  phone: "555-222-2222",hobi:"Shoping"},
        { photo: 'resources/c.png',npm: '173510514',name: 'Dianna', email: "deanna.troi@student.uir.ac.id",  phone: "555-333-3333",hobi:"Traveling"},
        { photo: 'resources/4.jpg',npm: '183510515',name: 'Rangga', email: "rangga.data@student.uir.ac.id",  phone: "555-444-4444",hobi:"Olahraga"},
        { photo: 'resources/2.jpg',npm: '193510512',name: 'Nadin', email: "nadin@student.uir.ac.id", phone: "555-555-5555", hobi:"Shoping"},
        { photo: 'resources/6.jpg',npm: '183510512',name: 'Abdul', email: "abdul@student.uir.ac.id", phone: "555-111-6666", hobi:"Traveling"},
        { photo: 'resources/3.jpg',npm: '173510512',name: 'Resi', email: "esi@student.uir.ac.id", phone: "555-111-7777", hobi:"Games"},
        { photo: 'resources/7.jpg',npm: '183510512',name: 'Andini', email: "dinian@student.uir.ac.id", phone: "555-888-1111", hobi:"Games"}
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
